# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
        ('note', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='categoriesnotes',
            options={'ordering': ['tree_id', 'lft']},
        ),
        migrations.AlterField(
            model_name='categoriesnotes',
            name='parent',
            field=mptt.fields.TreeForeignKey(related_name='child', null=True, blank=True, to='note.CategoriesNotes', unique=True),
        ),
    ]
