# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
        ('note', '0004_auto_20150622_0854'),
    ]

    operations = [
        migrations.AlterField(
            model_name='categoriesnotes',
            name='parent',
            field=mptt.fields.TreeForeignKey(related_name='child', blank=True, to='note.CategoriesNotes', null=True),
        ),
    ]
