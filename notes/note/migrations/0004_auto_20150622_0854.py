# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('note', '0003_auto_20150621_1345'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='categoriesnotes',
            options={},
        ),
        migrations.AlterField(
            model_name='categoriesnotes',
            name='parent',
            field=models.ForeignKey(related_name='Child', verbose_name=b'Parent', blank=True, to='note.CategoriesNotes', null=True),
        ),
    ]
