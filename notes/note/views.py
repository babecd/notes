#from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from note import serializers
from rest_framework.permissions import IsAuthenticated, IsAdminUser, IsAuthenticatedOrReadOnly
from note.models import Note, CategoriesNotes, LabelsNotes, MediaNotes
from rest_framework import viewsets
from note.permissions import IsAccountOwner
from django.contrib.auth.models import User
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework import status
from rest_framework import generics
# from django.shortcut import get_object_or_404


class CategoriesViewApi(viewsets.ModelViewSet, APIView):
    """
    You can handle categories
    """
    queryset = CategoriesNotes.objects.all()
    serializer_class = serializers.CategoriesSerializer


class AuthUserViewAPI(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)

    def get(self, request, format=None):
        content = {
            'user': unicode(request.user),
            'auth': unicode(request.auth),
        }
        return Response(content)


class NotesListViewApi(viewsets.ModelViewSet, APIView):
    """
    Manager of notes
    """
    queryset = Note.objects.all()
    serializer_class = serializers.NoteSerializer
    permission_classes = (IsAuthenticated, IsAccountOwner)

    def list(self, request, *args, **kwargs):
        queryset = Note.objects.filter(user_id=request.user.id)
        serializer = serializers.NoteSerializer(queryset, many=True)
        return Response(serializer.data)


class NotesDetailViewAPI(viewsets.ModelViewSet, APIView):
    """
    More detail about notes
    """
    serializer_class = serializers.NoteSerializer
    permission_classes = (IsAuthenticated, IsAccountOwner)
    queryset = Note.objects.all()
    lookup_field = "note__id"


class UsersView(viewsets.ModelViewSet, APIView):
    """
    All existing users
    """
    serializer_class = serializers.UserSerializer
    permission_classes = (IsAuthenticated, IsAdminUser,)
    queryset = User.objects.all()
    lookup_field = "id"


class LabelsViewApi(viewsets.ModelViewSet):
    """
    You can handle lablels
    """
    queryset = LabelsNotes.objects.all()
    serializer_class = serializers.LabelsSerializer


class MediaViewApi(viewsets.ModelViewSet, APIView):
    """
    """
    queryset = MediaNotes.objects.all()
    serializer_class = serializers.MediaSerializer
    permission_classes = (IsAuthenticated, IsAccountOwner,)
