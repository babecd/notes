from django.conf.urls import include, url, patterns
from note import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'list', views.NotesListViewApi)
router.register(r'accounts', views.UsersView)
router.register(r'caterories', views.CategoriesViewApi)
router.register(r'labls', views.LabelsViewApi)
router.register(r'media', views.MediaViewApi)

urlpatterns = patterns('',
                       url(r'^', include(router.urls)),
                       )
