from rest_framework import serializers
from note.models import Note, CategoriesNotes, LabelsNotes
from django.contrib.auth.models import User


class NoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Note
        fields = ['name', 'text', 'color',
                  'category', 'lable', 'user', 'media']


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'password',
                  'first_name', 'last_name', 'email')
        write_only_fields = ('password',)
        read_only_field = ('id',)

    def update(self, instance, validated_data):
        profile_data = validated_data.pop('password')
        user = super(UserSerializer, self).update(instance, validated_data)
        if profile_data:
            user.set_password(profile_data)
        return user

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name']
        )
        user.set_password(validated_data['password'])
        user.save()
        return user


class CategoriesSerializer(serializers.ModelSerializer):
    class Meta:
        model = CategoriesNotes
        fields = ['name', 'parent']


class LabelsSerializer(serializers.ModelSerializer):
    class Meta:
        model = LabelsNotes
        fields = ['lable']
