from django.db import models
from colorful.fields import RGBColorField
from django.contrib.auth.models import User
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel
import mptt


class CategoriesNotes(MPTTModel, models.Model):
    name = models.CharField('category', max_length=50, default='', unique=True)
    parent = TreeForeignKey('self', blank=True,
                            null=True, related_name='child')

    def __unicode__(self):
        return self.name
mptt.register(CategoriesNotes,)


class ColorsNotes(models.Model):
    code = RGBColorField()

    def __unicode__(self):
        return '{0}'.format(self.code)


class LabelsNotes(models.Model):
    lable = models.CharField(max_length=100)

    def __unicode__(self):
        return '{0}'.format(self.lable)


class MediaNotes(models.Model):
    media = models.FileField(upload_to='media/', blank=True, null=True)

    def __unicode__(self):
        return self.media.name


class Note(models.Model):
    name = models.CharField(max_length=30)
    text = models.TextField()
    color = models.ForeignKey(ColorsNotes)
    category = models.ManyToManyField(CategoriesNotes)
    lable = models.ManyToManyField(LabelsNotes)
    media = models.ManyToManyField(MediaNotes, blank=True, null=True)
    user = models.ForeignKey(User)

    def __unicode__(self):
        return '{0}: {1}'.format(self.name, self.text)
