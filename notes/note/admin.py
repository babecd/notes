from django.contrib import admin
from note.models import Note, ColorsNotes, CategoriesNotes, LabelsNotes, MediaNotes
from feincms.admin import tree_editor


class CategoryAdmin(tree_editor.TreeEditor):
    list_display = ('name',)


class NoteAdmin(admin.ModelAdmin):
    list_display = ['name', 'text']

admin.site.register(Note, NoteAdmin)
admin.site.register(ColorsNotes)
admin.site.register(CategoriesNotes, CategoryAdmin)
admin.site.register(LabelsNotes)
admin.site.register(MediaNotes)
