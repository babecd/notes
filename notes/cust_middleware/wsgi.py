from elasticsearch import Elasticsearch
from datetime import datetime

class SaveApiWsgi(object):

    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        if environ['PATH_INFO'].startswith('/api/'):
            return self.is_api(environ, start_response)
        else:
            return self.app(environ, start_response)

    def is_api(self, environ, start_response):
        es = Elasticsearch()
        try:
            request_body_size = int(environ.get('CONTENT_LENGTH', '0'))
        except ValueError:
            request_body_size = 0
        data = environ['wsgi.input'].read(request_body_size)
        foo = self.app(environ, start_response)
        try:
            resp = foo.data
        except AttributeError:
            resp = ''
        dt = datetime.now()
        doc = {
               'CONTENT_LENGTH': environ['CONTENT_LENGTH'],
               'CONTENT_TYPE': environ['CONTENT_TYPE'],
               'PATH_INFO': environ['PATH_INFO'],
               'body': data,
               'response': resp,
               'sessionid': environ['HTTP_COOKIE'].split(';')[1],
               'datetime': dt.isoformat(),
        }

        es.index(index=dt.strftime("logstash-%Y.%m.%d"), doc_type='request', body=doc, timestamp=dt.isoformat())
        # for key, val in environ.items():
        #     print '%s = %s' % (key, val)
        return foo
